# fibonaccis_api

Api rest to get fibonacci number for an index given

## Start 
To get started, just open a terminal on linux/mac or command line in windows, change directory where project is located, and run:
```
> python index.py
```
A server will start at `http://localhost` or `http://127.0.0.1` on port ``8080``

Open your favorite web browser and go to: [http://localhost:8080/fibonacci/6](http://localhost:8080/fibonacci/6) and you should get response with fibonacci value on the index indicated in url.

You can replace last number in url with any other number to indicate the index you want to obtain from fibonacci sequence.

## :dizzy: Python
A simple and strong programming language to develop for different areas, including the web.
It is well documented and robust, and maintainability is good and simple due to readability; plus it is simple to coding each block.
## :star2: Bottle
A micro framework for fast and simple Rest API with no dependecies

### why bottle
For a simple and fast development of the Rest API, this framework could help a lot, when the project is not very complex and small ins size.

## For future
In case, for whatever reason, this simple project grows; A framework change could be
made for a better perfomance. Choose from some others framworks made with __Python__. Some options are:

1. FastAPI
2. Flask

Another future improvement to the project is to include docker containers to maintain development standards with other collaborators.