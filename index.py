from bottle import Bottle, run
from App.Fibonacci import Fibonacci

app = Bottle()
fibonacci = Fibonacci()


@app.route('/fibonacci/<index:int>')
def get_by_index(index):
    return "{0}".format(fibonacci.find_by_index(index))


if __name__ == '__main__':
    run(app, host='127.0.0.1', port=8080)
