class Fibonacci:

    def find_by_index(self, index):
        try:
            if(index < 0):
                raise IndexError
            seq = [0, 1]
            if(index > 1):
                i = 0
                while(True):
                    seq.append(seq[i]+seq[i+1])
                    i += 1
                    if len(seq) > index:
                        break

            return seq[index]
        except IndexError:
            return "No value on index less than zero"
